require 'nokogiri'
require 'open-uri'

class FFNCrawler
  @queue= :crawler_queue
  
  def self.perform(story_id)
    story = Story.find(story_id)
    first_chapter = self.fetch_doc story.remote_id, 1
    first_chapter = self.parse_doc first_chapter
    
    self.update_story story, first_chapter
    self.create_chapter story_id, first_chapter
    
    if first_chapter[:chapters] > 1
      2.upto(first_chapter[:chapters]) do |chapter|
        sleep Random.rand(5).seconds
        
        next_chapter = self.fetch_doc story.remote_id, chapter
        next_chapter = self.parse_doc next_chapter
        
        self.create_chapter story_id, next_chapter
      end
    end
        
  end
  
  def self.update_story(story, data)
    story.title = data[:title_t]
    story.author = data[:author]
    story.save
  end
  
  def self.create_chapter(story_id, data)
    chapter = Chapter.where("story_id = ? AND chapter_id = ?", story_id, data[:chapter]).order('created_at DESC').limit(1)[0]
    
    if chapter.nil? or chapter.content != data[:content]
      chapter = Chapter.new
      chapter.chapter_id = data[:chapter]
      chapter.title = data[:chapter_title]
      chapter.story_id = story_id
      chapter.content = data[:content]
      chapter.save
    end
  end
  
  def self.fetch_doc(story_id, chapter_id)
    Nokogiri::HTML(open("http://www.fanfiction.net/s/#{story_id}/#{chapter_id}/").read)
  end
  
  def self.parse_doc(doc)
    data = Hash.new
    
    doc.search("script").each do |script|
      script.content.each_line do |line|
        match = /var (storyid|chapter|chapters|words|userid|title_t|summary|cat_title|datep|dateu|author) = ([^;]+);/.match(line)
        
        next unless match
        
        begin 
          data[match[1].to_sym] = Integer(match[2])
        rescue ArgumentError
          data[match[1].to_sym] = match[2].strip.gsub(/\A\'/m, '').gsub(/\'\Z/m, '')
        end
      end
    end
    
    data[:chapter_title] = doc.css('select[name="chapter"] > option[@selected="selected"]')[0].children[0].content
    data[:chapter_title] = data[:chapter_title][data[:chapter_title].index('.')+1..-1]
    data[:content] = doc.search('#storytext')[0]
    unless data[:content].search('div.a2a_kit').nil?
      data[:content].search('div.a2a_kit').remove
    end
    data[:content] = data[:content].children.to_html
    
    data
  end
end
